package net.phippy.web.user;

import lombok.extern.slf4j.Slf4j;
import net.phippy.web.open.model.ServletRequest;
import net.phippy.web.open.repo.RequestRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Controller
@RequestMapping("/user")
public class DashboardController {

    private final RequestRepository requestRepository;

    public DashboardController(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    @RequestMapping("/dashboard")
    public String dashboard(Model model) {
        return "user/dashboard";
    }
}
