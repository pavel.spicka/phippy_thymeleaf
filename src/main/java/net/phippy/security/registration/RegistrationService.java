package net.phippy.security.registration;

import lombok.extern.slf4j.Slf4j;
import net.phippy.email.EmailSender;
import net.phippy.security.appuser.AppUser;
import net.phippy.security.appuser.AppUserRole;
import net.phippy.security.appuser.AppUserService;
import net.phippy.security.exception.BadRequestException;
import net.phippy.security.registration.token.ConfirmationTokenService;
import net.phippy.security.registration.validator.EmailValidator;
import net.phippy.security.registration.validator.UsernameValidator;
import net.phippy.web.open.model.RegisterModel;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RegistrationService {

    private final AppUserService appUserService;
    private final EmailValidator emailValidator;
    private final UsernameValidator usernameValidator;
    private final ConfirmationTokenService confirmationTokenService;
    private final EmailSender emailSender;
    private final ServerProperties serverProperties;

    public RegistrationService(AppUserService appUserService, EmailValidator emailValidator, UsernameValidator usernameValidator, ConfirmationTokenService confirmationTokenService, EmailSender emailSender, ServerProperties serverProperties) {
        this.appUserService = appUserService;
        this.emailValidator = emailValidator;
        this.usernameValidator = usernameValidator;
        this.confirmationTokenService = confirmationTokenService;
        this.emailSender = emailSender;
        this.serverProperties = serverProperties;
    }

    public ResponseEntity<?> register(RegisterModel request) throws BadRequestException {

        if (request.getUsername() == null || request.getUsername().isEmpty()) {
            throw new BadRequestException("Username is empty.");
        }

        if (request.getEmail() == null ||
                request.getEmail().isEmpty() ||
                !request.getEmail().contains("@") ||
                !request.getEmail().contains(".")) {
            throw new BadRequestException("Email is wrong.");
        }

        if (request.getPassword() == null || request.getPassword().isEmpty()) {
            throw new BadRequestException("Password is empty.");
        }

        if (!usernameValidator.test(request.getUsername())) {
            throw new BadRequestException("Username is not available.");
        }

        if (!emailValidator.test(request.getEmail())) {
            throw new BadRequestException("Email is not available.");
        }

        try {
            emailSender.send(request.getEmail(), buildEmail(request.getUsername(),
                    "https://api.phippy.net/api/v1/registration/confirm?token=" +
                            appUserService.signUpUser(
                                    new AppUser(
                                            request.getUsername(),
                                            request.getEmail(),
                                            request.getPassword(),
                                            AppUserRole.USER
                                    ))
            ));
        } catch (IllegalStateException e){
            throw new BadRequestException("Failed to send email.");
        }

        return ResponseEntity.ok("Confirmed");
    }

    private String buildEmail(String name, String link) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n" +
                "<!-- saved from url=(0950)https://mail-attachment.googleusercontent.com/attachment/u/0/?ui=2&ik=f4c522cf58&attid=0.1&permmsgid=msg-f:1729117515411694625&th=17ff0f3811052421&view=att&disp=inline&realattid=f_l1jowprf0&ser=1&saddbat=ANGjdJ9V8bMcxCxWbpkQLnIjVc5T_rjT6k0FZeiqwImWgzQhvBD7FwXekkzfwm4uCh2U3vlku9JqmTLMKjwV_JNB5Jq_bDECx0fuCEOqvw94T9_nX4GZRFJKeaDLvexnTdxiKSS4_-iQ_NVPezaZahPSLm7jYCApLysFWoBt-w-s0kJv2JvVqhMb43heIe1K4lYocZ60NdgzMaT2qemE9BjHONVgGQWMvlhnAuxl2bIiMg4U6XdfvtaIyj0QY27q3oZgwvaip3LwhEO96QktBAgdby2YN5S_axaU4wePjWN7UbaWZf-oPdw2-vFeDejnGDKfZ21ooBBV0wOhUgTKehj2jdjVtsi3SZhYar2RPl2dr-W_NfheHx60Ne7vfkId2O4pcN89b7KuzeYOs2u11h4A7QiPl6UZr-JiabHzNCF9eAZ2FDxtG34lIwqze92XxWUsWTW6LoPE634OyFdUZTxaNToS-R254qxJY_eiG3mNOnt74FVbf1FQSLzR-gm2_JZ303B3EDvroVjQyu2qCaqVI76fFVhmQ5sg-VpSjbHGiUbBNd2j6-xh95HgZXrigIr_HYaJJkk4muK4QYSMZz2Ok10IA-IVC1IrhBJgNTlCa-s2SBGVE3T-D0vfflc05hGPWxl7wu9Soq8MovmQWpW7PEtoaTVEOz5DIftElAnCm0Ww04sfilCAebC8g_kAz4xq-4mG50xYAFtGSs7RTnItt_qmzVNw_wPrclOaYw -->\n" +
                "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><style></style></head><body>\n" +
                "  \n" +
                "    \n" +
                "  \n" +
                "  <div style=\"padding:0;margin:20px auto;border:none;font-family:&#39;Nunito&#39;,sans-serif;background-color:#ffffff;width:800px;height:fit-content;text-align:center;border-radius:10px\">\n" +
                "    <u></u>\n" +
                "        <h1 style=\"padding:0;margin:0;border:none;font-family:&#39;Nunito&#39;,sans-serif\">\n" +
                "            Confirm registration\n" +
                "        </h1>\n" +
                "        <p style=\"padding:0px 20px;margin:0;border:none;font-family:&#39;Nunito&#39;,sans-serif\">\n" +
                "            Confirmation for username " + name + "\n" +
                "        </p>\n" +
                "        <a class=\"m_button\" href=\"" + link + "\" style=\"padding:6px 14px;margin:20px auto 0px;border:none;font-family:&#39;Nunito&#39;,sans-serif;display:inline-block;background-color:rgb(0,71,1);color:#ffffff;font-size:22px;font-weight:600;text-decoration:none;border-radius:5px\" target=\"_blank\" rel=\"noreferrer\">\n" +
                "            CONFIRM\n" +
                "        </a>\n" +
                "    <u></u>\n" +
                "    <hr style=\"padding:0;margin:10px;border:0.5px solid rgb(222,222,222);font-family:&#39;Nunito&#39;,sans-serif;text-align:left\">\n" +
                "    <footer style=\"padding:10px;margin:0;border:none;font-family:&#39;Nunito&#39;,sans-serif;font-size:12px\">\n" +
                "        <p style=\"padding:0px 20px;margin:0;border:none;font-family:&#39;Nunito&#39;,sans-serif\">© 2022 Pavel Spicka</p>\n" +
                "        <a href=\"https://www.google.com/url?q=https://phippy.net/&amp;source=gmail-html&amp;ust=1649101788552000&amp;usg=AOvVaw3PlzEX7ZPmj9WDjb0PTJFP\" style=\"padding:0;margin:0;border:none;font-family:&#39;Nunito&#39;,sans-serif\" target=\"_blank\" rel=\"noreferrer\">www.phippy.net</a><br style=\"padding:0;margin:0;border:none;font-family:&#39;Nunito&#39;,sans-serif\">\n" +
                "    </footer>\n" +
                "  </div>\n" +
                "</body></html>";
    }
}
