package net.phippy.security.appuser;

public enum AppUserRole {
    USER,
    ADMIN
}
