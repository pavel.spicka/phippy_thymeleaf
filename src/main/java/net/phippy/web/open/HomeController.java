package net.phippy.web.open;

import lombok.extern.slf4j.Slf4j;
import net.phippy.security.exception.BadRequestException;
import net.phippy.security.registration.RegistrationService;
import net.phippy.web.open.model.RegisterModel;
import net.phippy.web.open.model.ServletRequest;
import net.phippy.web.open.repo.RequestRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

@Slf4j
@Controller
public class HomeController {

    private final RegistrationService registrationService;

    private final RequestRepository requestRepository;

    public HomeController(RegistrationService registrationService, RequestRepository requestRepository) {
        this.registrationService = registrationService;
        this.requestRepository = requestRepository;
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/projects")
    public String projects() {
        return "projects";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/register")
    public String register() {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerPOST(@ModelAttribute RegisterModel registerModel, Model model) {
        try {
            registrationService.register(registerModel);
            model.addAttribute("loginError", false);
            model.addAttribute("error", "");
            return "redirect:/";
        } catch (BadRequestException e) {
            model.addAttribute("loginError", true);
            model.addAttribute("error", e.getError());
        }
        return "register";
    }

    @GetMapping(value = "/img/profilePicture.jpg", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    ResponseEntity<byte[]> getImageProfilePicture() throws IOException {
        return new ResponseEntity<>(
                Objects.requireNonNull(getClass().getResourceAsStream("/templates/img/profilePicture.jpg")).readAllBytes(),
                HttpStatus.OK);
    }

    @GetMapping(value = "/img/favicon.ico", produces = "image/ico")
    public @ResponseBody
    ResponseEntity<byte[]> getImageFavicon() throws IOException {
        return new ResponseEntity<>(
                Objects.requireNonNull(getClass().getResourceAsStream("/templates/img/favicon.ico")).readAllBytes(),
                HttpStatus.OK);
    }

    @GetMapping(value = "/img/java.jpg", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    ResponseEntity<byte[]> getImageJava() throws IOException {
        return new ResponseEntity<>(
                Objects.requireNonNull(getClass().getResourceAsStream("/templates/img/java.jpg")).readAllBytes(),
                HttpStatus.OK);
    }

    @GetMapping(value = "/img/spring.jpg", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    ResponseEntity<byte[]> getImageSpring() throws IOException {
        return new ResponseEntity<>(
                Objects.requireNonNull(getClass().getResourceAsStream("/templates/img/spring.jpg")).readAllBytes(),
                HttpStatus.OK);
    }

    @GetMapping(value = "/img/thymeleaf.jpg", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    ResponseEntity<byte[]> getImageThymeleaf() throws IOException {
        return new ResponseEntity<>(
                Objects.requireNonNull(getClass().getResourceAsStream("/templates/img/thymeleaf.jpg")).readAllBytes(),
                HttpStatus.OK);
    }

    @GetMapping(value = "/img/bootstrap.jpg", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    ResponseEntity<byte[]> getImageBootstrap() throws IOException {
        return new ResponseEntity<>(
                Objects.requireNonNull(getClass().getResourceAsStream("/templates/img/bootstrap.jpg")).readAllBytes(),
                HttpStatus.OK);
    }
}
