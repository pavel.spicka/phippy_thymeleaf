package net.phippy.web.open.repo;

import net.phippy.web.open.model.ServletRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestRepository extends JpaRepository<ServletRequest, Long> {}
