package net.phippy.security.registration.validator;

import lombok.extern.slf4j.Slf4j;
import net.phippy.security.appuser.AppUserRepository;
import org.springframework.stereotype.Service;

import java.util.function.Predicate;

@Service
@Slf4j
public class UsernameValidator implements Predicate<String> {

    private final AppUserRepository appUserRepository;

    public UsernameValidator(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public boolean test(String username) {
        return appUserRepository.findByUsername(username).isEmpty();
    }
}
