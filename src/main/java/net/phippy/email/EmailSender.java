package net.phippy.email;

public interface EmailSender {
    void send(String to, String email);
}
