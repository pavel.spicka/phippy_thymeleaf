package net.phippy.web.open.model;

import lombok.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterModel {
    private String username;
    private String email;
    private String password;
    private String confirmPassword;
}
