package net.phippy.config;

import net.phippy.security.appuser.AppUserRole;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService uds;

    private final BCryptPasswordEncoder encoder;

    private final WebAccessDeniedHandler accessDeniedHandler;

    public WebSecurityConfig(UserDetailsService uds, BCryptPasswordEncoder encoder, WebAccessDeniedHandler accessDeniedHandler) {
        this.uds = uds;
        this.encoder = encoder;
        this.accessDeniedHandler = accessDeniedHandler;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(uds).passwordEncoder(encoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/api/v*/auth/**",
                        "/api/v*/registration/**",
                        "/swagger-ui.html",
                        "/swagger-ui/**",
                        "/",
                        "/login",
                        "/error",
                        "/register",
                        "/projects",
                        "/img/profilePicture.jpg",
                        "/img/favicon.ico",
                        "/img/java.jpg",
                        "/img/spring.jpg",
                        "/img/thymeleaf.jpg",
                        "/img/bootstrap.jpg")
                .permitAll()
                .antMatchers("/admin/**").hasAuthority(AppUserRole.ADMIN.name())
                .antMatchers("/user/**").hasAuthority(AppUserRole.USER.name())
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .successForwardUrl("/")
                .permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/")
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler);



    }

}
