package net.phippy.security.registration.validator;

import lombok.extern.slf4j.Slf4j;
import net.phippy.security.appuser.AppUserRepository;
import org.springframework.stereotype.Service;

import java.util.function.Predicate;

@Service
@Slf4j
public class EmailValidator implements Predicate<String> {

    private final AppUserRepository appUserRepository;

    public EmailValidator(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public boolean test(String email) {
        return appUserRepository.findByEmail(email).isEmpty();
    }
}
