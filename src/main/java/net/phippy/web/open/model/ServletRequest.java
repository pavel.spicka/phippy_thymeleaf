package net.phippy.web.open.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
@Builder
public class ServletRequest {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO) private Long id;
    private String remoteAddr;
}
